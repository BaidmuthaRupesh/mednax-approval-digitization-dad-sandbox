({
    submitforAproval : function(component, event, helper) {
        var recordId = component.get("v.recordId");
        var action  = component.get("c.ApprovalRecord");
        action.setParams({
            "RecordId": recordId
        });
        action.setCallback(this, function(a) {            
            var returnValue = a.getReturnValue();
            console.log('returnValue>>'+returnValue);
            var state = a.getState();
            if (state === "SUCCESS") {
                if(returnValue != null && returnValue !=''){
                    component.set("v.errorMsg", returnValue);
                    component.set("v.InitialApprovalMsg", false);
                    component.set("v.approvalErrorMsg", true);
                } else if(returnValue == null){
                    $A.get("e.force:closeQuickAction").fire();
                    $A.get('e.force:refreshView').fire();
                }
            } else if (state === "ERROR") {
                // Configure error toast
                let toastParams = {
                    title: "Error",
                    message: "Unknown error", // Default error message
                    type: "error"
                };
                // Pass the error message if any
                if (errors && Array.isArray(errors) && errors.length > 0) {
                    toastParams.message = errors[0].message;
                }
                // Fire error toast
                let toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams(toastParams);
                toastEvent.fire();
            }
        });
        $A.enqueueAction(action);
        
    },
    
    closeModel: function(component, event, helper) {
        // for Hide/Close Model,set the "isOpen" attribute to "Fasle"  
        component.set("v.isOpen", false);
        $A.get("e.force:closeQuickAction").fire();
    },
})