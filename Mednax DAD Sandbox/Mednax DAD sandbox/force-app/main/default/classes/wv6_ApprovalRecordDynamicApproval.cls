/*
* Company - Wave6
* Date - 5/18/2021
* Author - wave6
* Description -
* History -  New
*/
public class wv6_ApprovalRecordDynamicApproval {
    
    /********************************************************************************************************************************
     * Method:         ApprovalRecord
     * Description:    This method is used for Dynamic approval process for Approval Records
     * 					using lightning component & display dymanic error based on status of Approval Records
     * ******************************************************************************************************************************/
    
    @AuraEnabled
    public static String ApprovalRecord(Id RecordId){
        String result;
        Approval_Record__c ApprovalRecord = [Select Id,Pro_forma_Approval_Status__c
                                             from Approval_Record__c where Id =: RecordId];
        if(ApprovalRecord.Pro_forma_Approval_Status__c == 'Pending Approval' || ApprovalRecord.Pro_forma_Approval_Status__c == 'Rejected' ||
           ApprovalRecord.Pro_forma_Approval_Status__c == 'Recalled' || ApprovalRecord.Pro_forma_Approval_Status__c == 'Final Approval') {
               result = Label.Approval_Record_Error_Msg;
               return result;
           } else {
               wv6_ApprovalRecordDynamicApproval.submitForApproval(ApprovalRecord.Id);
               return null;
           }
    }
     /********************************************************************************************************************************
     * Method:         submitForApproval
     * Description:    This methods route Approval process to respective Approval records.
     * ******************************************************************************************************************************/
   
    public static void submitForApproval(id ApprovalRecordId){        
        // Create an approval request for  ApprovalRecord        
        Approval.ProcessSubmitRequest req = new Approval.ProcessSubmitRequest();          
        req.setObjectId(ApprovalRecordId);
        Approval.ProcessResult result = Approval.process(req);        
    }     
}