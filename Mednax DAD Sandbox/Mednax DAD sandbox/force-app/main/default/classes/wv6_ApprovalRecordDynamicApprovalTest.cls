/*
* Company - Wave6
* Date - 5/11/2021
* Author - wave6
* Description - Test class for wv6_ApprovalRecordDynamicApproval
* History -  New
*/
@istest
public class wv6_ApprovalRecordDynamicApprovalTest {
    static testMethod void testoppFinancialValidation_ApprovalRecord(){
        Id accountRecordType = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Service Location').getRecordTypeId();
        Account accRecord = new Account();
        accRecord.Name = 'TestWave6Account';
        accRecord.RecordTypeId = accountRecordType;
        accRecord.ShippingCountry = 'United States';
        accRecord.ShippingPostalCode = '56789';
        accRecord.ShippingState = 'Arizona';
        accRecord.ShippingStreet = 'street';
        accRecord.ShippingCity = 'city';
        insert accRecord;
        
        system.assertNotEquals(accRecord,null);
        Id oppRecordType = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Region Opportunity').getRecordTypeId();
        Opportunity oppRecord = new Opportunity();
        oppRecord.Name = 'Test Wave6 Opp';
        oppRecord.AccountId = accRecord.Id;
        oppRecord.CloseDate = date.today();
        oppRecord.Goal_Tracking_Prob__c = 20;
        oppRecord.StageName = 'Probable';
        oppRecord.Solutions__c = 'Maternal-Fetal Medicine';
        oppRecord.RecordTypeId = oppRecordType;
        insert oppRecord;
        system.assertNotEquals(oppRecord,null);
        
        Approval_Record__c approvalRecord = New Approval_Record__c();
        approvalRecord.Opportunity_Name__c = oppRecord.id;
        approvalRecord.Pro_forma_Approval_Status__c ='Not Submitted';
        insert approvalRecord;
        system.assertNotEquals(approvalRecord,null);
        Approval_Record__c approvalRecord2 = New Approval_Record__c();
        approvalRecord2.Opportunity_Name__c = oppRecord.id;
        approvalRecord2.Pro_forma_Approval_Status__c ='Pending Approval';
        insert approvalRecord2;
        system.assertNotEquals(approvalRecord,null);
        Test.startTest();
        wv6_ApprovalRecordDynamicApproval.ApprovalRecord(approvalRecord.Id);
        wv6_ApprovalRecordDynamicApproval.ApprovalRecord(approvalRecord2.Id);
        Test.stopTest();
    }
    
}